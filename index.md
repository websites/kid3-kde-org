---
title: Kid3 - Audio Tagger
layout: default
css-include: /css/main.css
---

<section id="kHeader" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="carousel-item-content pl-4 pr-4">
        <div class="slide-background" style="background-image: url(assets/img/ss_kde_app.png)"></div>
        <div class="carousel-text-overlay">
          <h1>Kid3</h1>
          <p>An Audio Tagger</p>
          <p><em>On Linux</em></p>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="carousel-item-content pl-4 pr-4">
        <div class="slide-background" style="background-image: url(assets/img/ss_mac_app.png)"></div>
        <div class="carousel-text-overlay">
          <h1>Kid3</h1>
          <p>An Audio Tagger</p>
          <p><em>On macOS</em></p>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="carousel-item-content pl-4 pr-4">
        <div class="slide-background" style="background-image: url(assets/img/ss_android_app.png)"></div>
        <div class="carousel-text-overlay">
          <h1>Kid3</h1>
          <p>An Audio Tagger</p>
          <p><em>On Android</em></p>
        </div>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#kHeader" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#kHeader" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</section>

<main class="content" id="content">
  <section class="container" markdown="1">

# **Kid3 - Audio Tagger**

{% assign version = site.data.releases[0].version %}

If you want to easily tag multiple MP3, Ogg/Vorbis, FLAC, Monkey's Audio, MPC, MP4/AAC, MP2, Opus, Speex, TrueAudio, WavPack, WMA/WMV, WAV and AIFF files (e.g. full albums) without typing the same information again and again and have control over both ID3v1 and ID3v2 tags, then Kid3 is the program you are looking for.

## Features

With Kid3 you can:

- Edit ID3v1.1 tags
- Edit all ID3v2.3 and ID3v2.4 frames
- Convert between ID3v1.1, ID3v2.3 and ID3v2.4 tags
- Edit tags in MP3, Ogg/Vorbis, DSF, FLAC, Monkey's Audio, MPC, MP4/AAC, MP2, Opus, Speex, TrueAudio, WavPack, WMA/WMV, WAV, AIFF files and tracker modules (MOD, S3M, IT, XM)
- Edit tags of multiple files, e.g. the artist, album, year and genre of all files of an album typically have the same values and can be set together
- Generate tags from filenames
- Generate tags from the contents of tag fields
- Generate filenames from tags
- Rename and create directories from tags
- Generate playlist files
- Automatically convert upper and lower case and replace strings
- Import from [gnudb.org](https://gnudb.org/), [MusicBrainz](https://musicbrainz.org/), [Discogs](https://discogs.com/), [Amazon](https://www.amazon.com/) and other sources of album data
- Export tags as CSV, HTML, playlists, Kover XML and in other formats
- Edit synchronized lyrics and event timing codes, import and export LRC files
- Automate tasks using QML/JavaScript, D-Bus or the command-line interface

## Requirements

Kid3 runs under Linux ([KDE](https://kde.org/) or only Qt), Windows, macOS and Android and uses [Qt](https://www.qt.io/), [id3lib](http://id3lib.sourceforge.net/), [libogg, libvorbis, libvorbisfile](https://www.xiph.org/), [libFLAC++, libFLAC](https://xiph.org/flac/), [TagLib](https://taglib.org/), [Chromaprint](https://acoustid.org/chromaprint).

## Information

- [The Kid3 Handbook](https://docs.kde.org/trunk5/en/kid3/kid3/)
  ([PDF](https://docs.kde.org/trunk5/en/kid3/kid3/kid3.pdf))<br/>
  Thanks to all those who translated the handbook:
  - ca: [El manual del Kid3](https://docs.kde.org/trunk5/ca/kid3/kid3/)
    ([PDF](https://docs.kde.org/trunk5/ca/kid3/kid3/kid3.pdf))
    (by Antoni Bella)
  - de: [Das Kid3 Handbuch](https://docs.kde.org/trunk5/de/kid3/kid3/)
    ([PDF](https://docs.kde.org/trunk5/de/kid3/kid3/kid3.pdf))
    (by Urs Fleisch)
  - it: [Manuale di Kid3](https://docs.kde.org/trunk5/it/kid3/kid3/)
    ([PDF](https://docs.kde.org/trunk5/it/kid3/kid3/kid3.pdf))
    (by Vincenzo Reale)
  - nl: [Het handboek van Kid3](https://docs.kde.org/trunk5/nl/kid3/kid3/)
    ([PDF](https://docs.kde.org/trunk5/nl/kid3/kid3/kid3.pdf))
    (by Freek de Kruijf)
  - pt: [O Manual do Kid3](https://docs.kde.org/trunk5/pt/kid3/kid3/)
    ([PDF](https://docs.kde.org/trunk5/pt/kid3/kid3/kid3.pdf))
    (by José Pires)
  - ru: [Руководство пользователя Kid3](https://docs.kde.org/trunk5/ru/kid3/kid3/)
    (by Мария Шикунова)
  - sv: [Handbok Kid3](https://docs.kde.org/trunk5/sv/kid3/kid3/)
    ([PDF](https://docs.kde.org/trunk5/sv/kid3/kid3/kid3.pdf))
    (by Stefan Asserhäll)
  - uk: [Підручник з Kid3](https://docs.kde.org/trunk5/uk/kid3/kid3/)
    ([PDF](https://docs.kde.org/trunk5/uk/kid3/kid3/kid3.pdf))
    (by Yuri Chornoivan)
- The [KDE Gitlab](https://invent.kde.org/multimedia/kid3/) hosts the source code repository and issues for help.
- At the [Kid3 project page at SourceForge](https://sourceforge.net/projects/kid3/) you can download all releases of Kid3.
- The [Kid3 page at linux-apps.org](https://www.linux-apps.com/p/1126630/) offers the possibility to rate Kid3 and to submit comments.
- The [Kid3 page at Open HUB](https://www.openhub.net/p/kid3) offers code metrics and the possibility to rate Kid3 and to submit comments.

## License

Kid3 is open source software licensed under the [GNU General Public License (GPL)](https://www.gnu.org/licenses/licenses.html#GPL).

## Download

- Sourcecode [kid3-{{ version }}.tar.gz](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}.tar.gz?download)
  [(PGP Signature)](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}.tar.gz.sig?download)
- Ubuntu users can add the [PPA for Kid3](https://launchpad.net/~ufleisch/+archive/ubuntu/kid3):
  ```bash
  sudo add-apt-repository ppa:ufleisch/kid3
  sudo apt-get update
  sudo apt-get install kid3     # KDE users
  sudo apt-get install kid3-qt  # without KDE dependencies
  sudo apt-get install kid3-cli # for the command-line interface
  ```
- Linux [Flatpak at Flathub](https://flathub.org/apps/details/org.kde.kid3)
- Linux binaries (64-bit) [kid3-{{ version }}-Linux.tgz](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-Linux.tgz?download)
  [(PGP Signature)](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-Linux.tgz.sig?download)
- Windows binaries (64-bit) [kid3-{{ version }}-win32-x64.zip](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-win32-x64.zip?download)
  [(PGP Signature)](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-win32-x64.zip.sig?download)
  , or alternatively, Qt 5 binaries for Windows 7 and earlier [kid3-{{ version }}-win32-x64-Qt5.zip](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-win32-x64-Qt5.zip?download)
  [(PGP Signature)](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-win32-x64-Qt5.zip.sig?download)
- macOS binaries [kid3-{{ version }}-Darwin-amd64.dmg](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-Darwin-amd64.dmg?download)
  [(PGP Signature)](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-Darwin-amd64.dmg.sig?download)
  , [kid3-{{ version }}-Darwin-arm64.dmg](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-Darwin-arm64.dmg?download)
  [(PGP Signature)](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-Darwin-arm64.dmg.sig?download)
  , or alternatively, Qt 5 binaries for macOS before Big Sur (11) [kid3-{{ version }}-Darwin-Qt5.dmg](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-Darwin-Qt5.dmg?download)
  [(PGP Signature)](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-Darwin-Qt5.dmg.sig?download)
- Android package [kid3-{{ version }}-android-armeabi-v7a.apk](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-android-armeabi-v7a.apk?download)
  [(PGP Signature)](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-android-armeabi-v7a.apk.sig?download)
  , [kid3-{{ version }}-android-arm64-v8a.apk](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-android-arm64-v8a.apk?download)
  [(PGP Signature)](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-android-arm64-v8a.apk.sig?download)
- [SHA256 sums](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-sha256sum.txt?download) for all release files
  [(PGP Signature)](https://prdownloads.sourceforge.net/kid3/kid3-{{ version }}-sha256sum.txt.sig?download)

The PGP signature can be verified on [pgp.mit.edu](https://pgp.mit.edu/pks/lookup?search=ufleisch%40users.sourceforge.net&op=index&exact=on)
or [keys.mailvelope.com](https://keys.mailvelope.com/pks/lookup?op=get&search=ufleisch%40users.sourceforge.net).

##### Thanks to all those who created the following packages:

- Windows [Chocolatey Package](https://chocolatey.org/packages/kid3/)
- Windows [Scoop Package](https://github.com/lukesampson/scoop-extras/blob/master/bucket/kid3.json)
- macOS [Homebrew Package](https://formulae.brew.sh/cask/kid3)
- Android [F-Droid Package](https://f-droid.org/en/packages/net.sourceforge.kid3/)
- Package for [Arch Linux](https://www.archlinux.org/packages/community/x86_64/kid3/) (by Jaroslav Lichtblau)
- Packaging of the KDE Framework version for [KaOS](http://kaosx.tk/packages/index.php?act=search&subdir=apps&searchpattern=kid3)
- Package for [Chakra](https://rsync.chakralinux.org/packages/desktop/x86_64/) (by Neophytos Kolokotronis)
- [kid3](https://packages.debian.org/sid/kid3) and [kid3-qt](https://packages.debian.org/sid/kid3-qt) for Debian Unstable (by Mark Purcell, Patrick Matthäi)
- [kid3](https://packages.ubuntu.com/kid3) and [kid3-qt](https://packages.ubuntu.com/kid3-qt) for Ubuntu
- Package for [Mageia](http://madb.mageia.org/package/show/name/kid3/release/cauldron/arch/x86_64) (by daviddavid)
- [Overview](https://build.opensuse.org/package/show/multimedia:apps/kid3) of packages for openSUSE
- [kid3](http://packman.links2linux.org/package/kid3) for openSUSE (by Packman)
- [Gentoo Package](https://packages.gentoo.org/packages/media-sound/kid3) (by Johannes Huber)
- [Overview](https://apps.fedoraproject.org/packages/kid3/) of packages for Fedora (by Hedayat Vatankhah)
- [kid3](https://openmamba.org/it/pacchetti/?tag=devel&pkg=kid3.x86_64) for openmamba (by Marco Bellezza)
- Package for [Alt Linux](http://www.sisyphus.ru/ru/srpm/Sisyphus/kde5-kid3) (by Sergey V Turchin)
- Port for [FreeBSD](https://www.freshports.org/audio/kid3-qt5/) (by Max Brazhnikov)
- Port for [NetBSD](http://pkgsrc.se/audio/kid3) (by Thomas Klausner)
- Port for [OpenBSD](https://openports.se/multimedia/kid3) (by Vadim Zhukov)

## Git

To get the source code repository, fetch it using Git

```bash
git clone git@invent.kde.org:multimedia/kid3.git
mkdir build
cd build
cmake ../kid3
```

Or browse the Kid3 code on [KDE Projects](https://invent.kde.org/multimedia/kid3/).

Then you can build Kid3 with the usual `cmake` and `make` commands or build an RPM or deb package.

Development snapshot binaries for Linux, Windows and Mac are available in the [development folder](https://sourceforge.net/projects/kid3/files/kid3/development/).

A nightly Flatpak build of Kid3 is available in the KDE Flatpak repository.

```bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --if-not-exists kdeapps https://distribute.kde.org/kdeapps.flatpakrepo
flatpak install kdeapps org.kde.kid3
flatpak run org.kde.kid3//master
```

## History

| Date                            | Release           | Changes           |
| ------------------------------- | ----------------- | ----------------- |
| ------------------------------- | ----------------- | ----------------- |
{% for rel in site.data.releases -%}
| {{ rel.date | date_to_string }} | {{ rel.version }} | {{ rel.changes }} |
{% endfor -%}
{: .changelog-table }

## Support

- If you found a bug, create a [bug report](https://bugs.kde.org/enter_bug.cgi?format=guided&product=kid3).
- If you need a feature, create a [new wishlist item](https://bugs.kde.org/enter_bug.cgi?format=guided&product=kid3&bug_severity=wishlist).
- Ask for help by creating a [new issue](https://invent.kde.org/multimedia/kid3/issues/new).
- Contact the author: Urs Fleisch [`<ufleisch at users.sourceforge.net>`](mailto:ufleisch%20at%20users.sourceforge.net).
